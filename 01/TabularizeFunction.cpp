#include <iostream>
#include <cmath>

using namespace std;

int main() {
    double x_min;
    double x_max;
    double num_steps;

    cout << "Please enter Value for x_min: ";
    cin >> x_min;
    cout << "Please enter value for x_max: ";
    cin >> x_max;
    cout << "Please enter value for num_steps: ";
    cin >> num_steps;

    for(int i = 0; i < num_steps; i++) {
	double x = i * (x_max - x_min) / (num_steps - 1);
	double y = sqrt(2 * x) * exp(-x / 2);
	cerr << x << "\t" << y << endl;
    }
}
