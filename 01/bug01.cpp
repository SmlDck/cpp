/********************************************************************************
 *
 * description:       a program to add up numbers from 1 to n
 * 
 * compile:           $ g++ bug01.cpp -o bug01
 *
 * expected result:   $ ./bug01
 *                    Enter a number: 11
 *                    The sum up to 11 is: 66             
 *
 * Copyright (C) October 2014               Stefan Harfst (University Oldenburg)
 * This program is made freely available with the understanding that every copy
 * of this file must include this header and that it comes without any WITHOUT
 * ANY WARRANTY.
 ********************************************************************************/
#include<iostream>

using namespace std;

int main()
{
  int n;
  // Initially setting the sum value to 0
  int sum = 0;

  cout << "Enter a number: ";
  cin >> n;

  // The loop needs to stop at n excatly
  for (int i=0; i<=n; ++i)
  {
    sum += i;
  }

  cout << "The sum up to " << n << " is: " << sum << endl; 
}
