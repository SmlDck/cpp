#define A_DEFAULT 1103515245ULL
#define C_DEFAULT 12345ULL
#define M_DEFAULT pow(2,31)
#include <iostream>
#include "classRandom.h"

using namespace std;

uInt64 checkPeriod(LCG& rnd)
{
    uInt64 First = rnd(), count=1;
    while (First != rnd())
	count++;
    return count;
}

int main()
{
    LCG rnd(9ULL, 7ULL, 16ULL);
    uInt64 period = checkPeriod(rnd);
    cout << "For rnd(9, 7, 16) the period is " << period << " iterations." << endl;
    for(uInt64 i = 0; i <= period; i++)
	cout << i << ". " << rnd() << endl;

    rnd = LCG(9ULL, 6ULL, 16ULL);
    period = checkPeriod(rnd);
    cout << "For rnd(9, 6, 16) the period is " << period << " iterations." << endl;    
    for(uInt64 i = 0; i <= period; i++)
	cout << i << ". " << rnd() << endl;
    return 0;

}
