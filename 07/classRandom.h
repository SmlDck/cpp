#ifndef _CLASSLCG_H_
#define _CLASSLCG_H_
#include <ctime>
#include <cmath>

using namespace std;
using uInt64 = unsigned long long int;

class LCG
{
    private:
	uInt64 a, c, m, x;

    public:
	LCG() : a(A_DEFAULT), c(C_DEFAULT), m(M_DEFAULT), x((uInt64)time(NULL)) {}
	LCG(uInt64 ain, uInt64 cin, uInt64 min) : a(ain), c(cin), m(min) { x = 0;  }

	uInt64 operator() ()
	{
	    uInt64 result = ((a * x + c) % m);
	    setSeed(result);
	    return result;
	}

	void setSeed(uInt64 newSeed)
	{
	    x = newSeed;
	}

};
#endif

