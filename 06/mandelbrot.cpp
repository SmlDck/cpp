#include <iostream>
#include "classComplex.h"
#include <fstream>
#define N 500.0
#define M 300.0
#define nMAX 1000.0
#define xStart -2.5
#define xEnd 1
#define yStart -1.0
#define yEnd 1.0


using namespace std;

int main()
{
    ofstream file("mandelbrot.dat");
    Complex c, z;

    for (double i = xStart; i < xEnd; i += (abs(xEnd - xStart) / N))
    {
        for (double j = yStart; j < yEnd; j += (abs(yEnd - yStart) / M))
        {
            int k;
            double color;
            c = Complex(i, j);
            z = Complex();
            for (k = 0; k < nMAX && z.length() < 2; k++)
            {
                z = (z * z) + c;
            }
            cout << k << endl;
            if (k >= nMAX)
                color = 0;
            else color = ((nMAX - k) / nMAX);
            file << i << " " << j  << " " << color << endl;
        }
    }
    file.close();
    return 0;
}

/*
Plotting the mandelbrot.dat results in an image of the Mandelbrot set.
Every z_m in the Set is colored black. After nMAX iterations we assume z_m to be in the set.
*/